import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class AllMarriages {
    private List<Marriage> listOfMarriages = new ArrayList<>();

    public AllMarriages(List<Person> listOfAllPersons) {
        List<Person> listOfMarriagesAsPerson = new ArrayList<>();
        for (Person person : listOfAllPersons) {
            if (!Objects.equals(person.getStatus(), "S")) {
                listOfMarriagesAsPerson.add(person);
            }
        }
        for (int i = 0; i < listOfMarriagesAsPerson.size(); i++) {
            Marriage marriage = new Marriage(listOfMarriagesAsPerson.get(i), listOfMarriagesAsPerson.get(i + 1));
            listOfMarriages.add(marriage);
            i++;
        }
    }

    public List<Marriage> getListOfMarriages() {
        return listOfMarriages;
    }

}
