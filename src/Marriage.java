public class Marriage {
    private Person husband;
    private Person wife;

    public Marriage(Person husband, Person wife) {
        this.husband = husband;
        this.wife = wife;
    }

    public Person getHusband() {
        return husband;
    }


    public Person getWife() {
        return wife;
    }
}
