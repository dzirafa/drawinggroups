import java.util.ArrayList;
import java.util.List;

public class MarriageTogetherDrawing extends Drawing {
    private List<Group> allGroups = new ArrayList<>();

    @Override
    public List<Group> drawingToGroups(int quantityOfGroups, List<Person> list) {
        AllMarriages allMarriages = new AllMarriages(list);
        AllSingle allSingle = new AllSingle(list);

        int[] randomArrayMarriages = arrayRand(allMarriages.getListOfMarriages().size());
        int[] randomArraySingles = arrayRand(allSingle.getListOfAllSingle().size());

        int divisionMarriages = randomArrayMarriages.length / quantityOfGroups;
        int moduloMarriages = randomArrayMarriages.length % quantityOfGroups;

        int divisionSingles = randomArraySingles.length / quantityOfGroups;

        for (int i = 0; i < quantityOfGroups; i++) {
            int groupNumber = i + 1;
            Group group = new Group(groupNumber);
            System.out.println("Group " + groupNumber);
            // Case of groups with more marriages
            if (moduloMarriages != 0) {
                if (i < moduloMarriages) {
                    // Adding marriages
                    for (int j = i * (divisionMarriages + 1); j <= i * (divisionMarriages + 1) + divisionMarriages; j++) {
                        group.addToTheGroup(allMarriages.getListOfMarriages()
                                .get(randomArrayMarriages[j] - 1).getHusband());
                        System.out.println(allMarriages.getListOfMarriages()
                                .get(randomArrayMarriages[j] - 1).getHusband());
                        group.addToTheGroup(allMarriages.getListOfMarriages()
                                .get(randomArrayMarriages[j] - 1).getWife());
                        System.out.println(allMarriages.getListOfMarriages()
                                .get(randomArrayMarriages[j] - 1).getWife());
                    }
                    //Adding singles
                    for (int j = 0; j < (divisionSingles + 1); j++) {
                        if ((i + j * quantityOfGroups + 2 * (quantityOfGroups - moduloMarriages))
                                < allSingle.getListOfAllSingle().size()) {
                            group.addToTheGroup(allSingle.getListOfAllSingle().get(randomArraySingles
                                    [i + j * quantityOfGroups + 2 * (quantityOfGroups - moduloMarriages)] - 1));
                            System.out.println(allSingle.getListOfAllSingle().get(randomArraySingles
                                    [i + j * quantityOfGroups + 2 * (quantityOfGroups - moduloMarriages)] - 1));
                        }
                    }
                    allGroups.add(group);
                } else {
                    // Case of groups with fewer marriages
                    // Adding marriages
                    for (int j = (i * (divisionMarriages + 1) - (i - moduloMarriages));
                         j < (i * (divisionMarriages + 1) - (i - moduloMarriages)) + divisionMarriages; j++) {
                        group.addToTheGroup(allMarriages.getListOfMarriages()
                                .get(randomArrayMarriages[j] - 1).getHusband());
                        System.out.println(allMarriages.getListOfMarriages()
                                .get(randomArrayMarriages[j] - 1).getHusband());
                        group.addToTheGroup(allMarriages.getListOfMarriages()
                                .get(randomArrayMarriages[j] - 1).getWife());
                        System.out.println(allMarriages.getListOfMarriages()
                                .get(randomArrayMarriages[j] - 1).getWife());
                    }
                    //Adding first single - first step to level fewer marriages
                    if ((i - moduloMarriages) < randomArraySingles.length) {
                        group.addToTheGroup(allSingle.getListOfAllSingle()
                                .get(randomArraySingles[i - moduloMarriages] - 1));
                        System.out.println(allSingle.getListOfAllSingle()
                                .get(randomArraySingles[i - moduloMarriages] - 1));
                    }
                    //Adding second single - second step to level fewer marriages
                    if ((i - moduloMarriages + quantityOfGroups - moduloMarriages) < randomArraySingles.length) {
                        group.addToTheGroup(allSingle.getListOfAllSingle().get(randomArraySingles[i - moduloMarriages
                                + quantityOfGroups - moduloMarriages] - 1));
                        System.out.println(allSingle.getListOfAllSingle().get(randomArraySingles[i - moduloMarriages
                                + quantityOfGroups - moduloMarriages] - 1));
                    }
                    //Adding singles
                    for (int j = 0; j < (divisionSingles + 1); j++) {
                        if ((i + j * quantityOfGroups + 2 * (quantityOfGroups - moduloMarriages))
                                < allSingle.getListOfAllSingle().size()) {
                            group.addToTheGroup(allSingle.getListOfAllSingle().get(randomArraySingles
                                    [i + j * quantityOfGroups + 2 * (quantityOfGroups - moduloMarriages)] - 1));
                            System.out.println(allSingle.getListOfAllSingle().get(randomArraySingles
                                    [i + j * quantityOfGroups + 2 * (quantityOfGroups - moduloMarriages)] - 1));
                        }
                    }
                    allGroups.add(group);
                }
            } else {
                // Case of groups with equal number of marriages
                // Adding marriages
                for (int j = (i * divisionMarriages); j < (i * divisionMarriages + divisionMarriages); j++) {
                    group.addToTheGroup(allMarriages.getListOfMarriages().get(randomArrayMarriages[j] - 1).getHusband());
                    System.out.println(allMarriages.getListOfMarriages().get(randomArrayMarriages[j] - 1).getHusband());
                    group.addToTheGroup(allMarriages.getListOfMarriages().get(randomArrayMarriages[j] - 1).getWife());
                    System.out.println(allMarriages.getListOfMarriages().get(randomArrayMarriages[j] - 1).getWife());
                }
                //Adding singles
                for (int j = 0; j < (divisionSingles + 1); j++) {
                    if ((i + j * quantityOfGroups) < allSingle.getListOfAllSingle().size()) {
                        group.addToTheGroup(allSingle.getListOfAllSingle()
                                .get(randomArraySingles[i + j * quantityOfGroups] - 1));
                        System.out.println(allSingle.getListOfAllSingle()
                                .get(randomArraySingles[i + j * quantityOfGroups] - 1));
                    }
                }
                allGroups.add(group);
            }
        }
        return allGroups;
    }

}
