import java.util.ArrayList;
import java.util.List;


public class SimpleDrawing extends Drawing {
    private List<Group> allGroups = new ArrayList<>();

    @Override
    public List<Group> drawingToGroups(int quantityOfGroups, List<Person> list) {
        int[] randomArray = arrayRand(list.size());
        int division = randomArray.length / quantityOfGroups;
        int modulo = randomArray.length % quantityOfGroups;
        for (int i = 0; i < quantityOfGroups; i++) {
            int groupNumber = i + 1; // Group number starts from 1.
            Group group = new Group(groupNumber);
            System.out.println("Group " + groupNumber);
            if (i < modulo) {   // In the first groups we place more people (number of people
                // equal to the result of dividing the number of people by the number of groups
                // with one person added resulting from the rest of the division)
                for (int j = 0; j < division; j++) {
                    int idOfPersonInteger = randomArray[i * division + j] - 1; // "-1" because of index of randomArray
                    group.addToTheGroup(list.get(idOfPersonInteger));
                    System.out.println(list.get(idOfPersonInteger).toString());
                }
                int idOfPersonModulo = randomArray[randomArray.length - modulo + i] - 1; // "-1" because of index of
                // randomArray
                group.addToTheGroup(list.get(idOfPersonModulo));
                System.out.println(list.get(idOfPersonModulo).toString());
            } else {    // In the last groups we place less people (number of people
                // equal to the result of dividing the number of people by the number of groups
                for (int j = 0; j < division; j++) {
                    int idOfPersonInteger = randomArray[i * division + j] - 1; // "-1" because of index of randomArray
                    group.addToTheGroup(list.get(idOfPersonInteger));
                    System.out.println(list.get(idOfPersonInteger).toString());
                }
            }
            allGroups.add(group);
        }
        return allGroups;
    }
}
