import java.net.URISyntaxException;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class UI {

    private int choice = 0;

    public UI() throws URISyntaxException {
        menu();
    }

    public void startMenu() {
        System.out.println("1. Display all persons");
        System.out.println("2. Display all singles");
        System.out.println("3. Display all marriages");
        System.out.println("4. Draw groups");
        System.out.println("0. Exit");
    }

    public void drawingMenu() {
        System.out.println("1. Simple drawing");
        System.out.println("2. Marriage together drawing");
        System.out.println("0. Exit");
    }

    public void menu() throws URISyntaxException {
        do {
            startMenu();
            getUserChoice();

            switch (choice) {
                case 1:
                    displayAllPersons();
                    break;
                case 2:
                    displayAllSingles();
                    break;
                case 3:
                    displayAllMariages();
                    break;
                case 4:
                    drawingMenu();
                    getUserChoice();
                    typeOfDraw();
                case 0:
                    System.exit(0);
                    break;
                default:
                    break;
            }
        } while (choice != 0);
    }

    public void typeOfDraw() throws URISyntaxException {
        switch (choice) {
            case 1:
                makeSimpleDrawing();
                menu();
                break;
            case 2:
                makeMarriageTogetherDrawing();
                menu();
                break;
            default:
                break;
        }
    }

    /**
     * Gets the the option number chosen by the user.
     *
     * @return the option number
     * @throws InputMismatchException if user choose not a number (int)
     */
    public int getUserChoice() {
        Scanner scanner = new Scanner(System.in);
        try {
            choice = scanner.nextInt();
        } catch (InputMismatchException e) {
            System.out.println("Invalid value! Choose available option!");
            getUserChoice();
        }
        return choice;
    }

    /**
     * Displays all groups (number of groups indicated by the user) after simple drawing
     * and saves results as the CSV file.
     */
    public void makeSimpleDrawing() throws URISyntaxException {
        System.out.println("Enter the number of groups:");
        int numberOfGroups = getUserChoice();
        List<Person> listOfPersons = new AllPersons().getListOfAllPersons();
        while (numberOfGroups > listOfPersons.size()) {
            System.out.println("The number of group is greater then the number of persons from" +
                    "the list. Enter the number of groups");
            numberOfGroups = getUserChoice();
        }
        SimpleDrawing simpleDrawing = new SimpleDrawing();
        simpleDrawing.printGroupsToFile(simpleDrawing.drawingToGroups(numberOfGroups, listOfPersons));
    }

    /**
     * Displays all groups (number of groups indicated by the user) after drawing that marriages are in the same group
     * and saves results as the CSV file.
     */
    private void makeMarriageTogetherDrawing() throws URISyntaxException {
        System.out.println("Enter the number of groups:");
        int numberOfGroups = getUserChoice();
        List<Person> listOfPersons = new AllPersons().getListOfAllPersons();
        while (numberOfGroups > listOfPersons.size()) {
            System.out.println("The number of group is greater then the number of persons from" +
                    "the list. Enter the number of groups");
            numberOfGroups = getUserChoice();
        }
        MarriageTogetherDrawing marriageTogetherDrawing = new MarriageTogetherDrawing();
        marriageTogetherDrawing.printGroupsToFile(marriageTogetherDrawing.drawingToGroups(numberOfGroups,
                listOfPersons));
    }

    /**
     * Displays all persons from the list of persons from AllPersons class
     */
    public void displayAllPersons() throws URISyntaxException {
        List<Person> listOfPersons = new AllPersons().getListOfAllPersons();
        for (Person person : listOfPersons) {
            System.out.println(person.toString());
        }
        System.out.println("Quantity of all persons: " + listOfPersons.size());
    }

    /**
     * Displays all persons from the list of persons from AllSingles class
     */
    public void displayAllSingles() throws URISyntaxException {
        List<Person> listOfPersons = new AllPersons().getListOfAllPersons();
        List<Person> listOfSingles = new AllSingle(listOfPersons).getListOfAllSingle();
        for (Person person : listOfSingles) {
            System.out.println(person.toString());
        }
        System.out.println("Quantity of singles: " + listOfSingles.size());
    }

    /**
     * Displays all persons from the list of persons from AllMarriages class
     */
    public void displayAllMariages() throws URISyntaxException {
        List<Person> listOfPersons = new AllPersons().getListOfAllPersons();
        List<Marriage> listOfMarriages = new AllMarriages(listOfPersons).getListOfMarriages();
        for (Marriage marriage : listOfMarriages) {
            System.out.println(marriage.getHusband().toString());
            System.out.println(marriage.getWife().toString());
        }
        System.out.println("Quantity of marriages: " + listOfMarriages.size());
    }

}
