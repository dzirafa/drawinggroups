import java.util.ArrayList;
import java.util.List;

public class Group {
    private int numberOfTheGroup;
    private List<Person> list = new ArrayList<>();

    public Group(int numberOfTheGroup) {
        this.numberOfTheGroup = numberOfTheGroup;
    }

    public int getNumberOfTheGroup() {
        return numberOfTheGroup;
    }

    public List<Person> getList() {
        return list;
    }

    public void addToTheGroup(Person person) {
        list.add(person);
    }
}
