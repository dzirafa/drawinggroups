public class Person {
    private int id;

    private String firstName;

    private String secondName;
    private String status;
    private String participation;

    public Person(int id, String firstName, String secondName, String status, String participation) {
        this.id = id;
        this.firstName = firstName;
        this.secondName = secondName;
        this.status = status;
        this.participation = participation;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public String getStatus() {
        return status;
    }

    public int getId() {
        return id;
    }

    public String getParticipation() {
        return participation;
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", secondName='" + secondName + '\'' +
                ", status='" + status + '\'' +
                ", participation='" + participation + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Person person = (Person) o;

        if (id != person.id) return false;
        if (firstName != null ? !firstName.equals(person.firstName) : person.firstName != null) return false;
        if (secondName != null ? !secondName.equals(person.secondName) : person.secondName != null) return false;
        if (status != null ? !status.equals(person.status) : person.status != null) return false;
        return participation != null ? participation.equals(person.participation) : person.participation == null;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + (secondName != null ? secondName.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (participation != null ? participation.hashCode() : 0);
        return result;
    }
}
