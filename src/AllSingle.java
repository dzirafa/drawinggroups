import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class AllSingle {
    private List<Person> listOfAllSingle = new ArrayList<>();

    public AllSingle(List<Person> listOfAllPersons) {
        for (Person person : listOfAllPersons) {
            if (Objects.equals(person.getStatus(), "S")) {
                listOfAllSingle.add(person);
            }
        }
    }

    public List<Person> getListOfAllSingle() {
        return listOfAllSingle;
    }
}
