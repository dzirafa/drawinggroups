import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Random;

public abstract class Drawing {

    public abstract List<Group> drawingToGroups(int quantityOfGroups, List<Person> list);

    /**
     * Prints all groups: FirstName and SecondName of Persons to CSV file.
     *
     * @throws FileNotFoundException if the csv file was not created
     */
    public void printGroupsToFile(List<Group> groups) throws URISyntaxException {
        String path = Drawing.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath();
        if (path.contains("losowanieGrup.jar")) {
            path = path.replace("losowanieGrup.jar", "groups.csv");
        } else {
            path = "out/groups.csv";
        }
        try (PrintStream file = new PrintStream(path)) {
            for (int i = 0; i < groups.size(); i++) {
                file.println("Group " + groups.get(i).getNumberOfTheGroup() + " - " + groups.get(i).getList().size() + " persons");
                for (int j = 0; j < groups.get(i).getList().size(); j++) {
                    file.println(groups.get(i).getList().get(j).getFirstName() + "; "
                            + groups.get(i).getList().get(j).getSecondName());
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Creates an int[] array with the dimension maxNum with randomly drawn numbers from 1 to maxNum.
     *
     * @return the int[] array with random numbers from 1 to maxNum
     */
    public int[] arrayRand(int maxNum) {
        int[] randomArray = new int[maxNum];
        int randomNumber;
        Random randomGenerator = new Random();
        for (int i = 0; i < maxNum; i++) {
            do {
                randomNumber = randomGenerator.nextInt(maxNum + 1);
            } while (isInArray(randomNumber, randomArray));
            randomArray[i] = randomNumber;
        }
        return randomArray;
    }

    /**
     * Check if int number "n" is in int[] array.
     *
     * @return true if the int number "n" is in int[] array
     */
    private static boolean isInArray(int n, int[] array) {
        for (int anArray : array) {
            if (anArray == n)
                return true;
        }
        return false;
    }
}
