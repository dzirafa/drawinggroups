import java.io.*;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;

public class AllPersons {

    private List<Person> listOfAllPersons = new ArrayList<>();

    public AllPersons() throws URISyntaxException {
        String path = AllPersons.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath();
        if (path.contains("losowanieGrup.jar")) {
            path = path.replace("losowanieGrup.jar", "enterList.csv");
        } else {
            path = "data/enterList.csv";
        }
        try (Scanner scanner = new Scanner(new File(path))) {
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                String[] split = line.split(";");
                Person person = new Person(Integer.parseInt(split[0]), split[1], split[2], split[3], split[4]);
                if (Objects.equals(person.getParticipation(), "T")) {
                    listOfAllPersons.add(person);
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public List<Person> getListOfAllPersons() {
        return listOfAllPersons;
    }

}
